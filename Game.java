import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

//siam,nafiz fuad 17-33438-1

public class Game extends JFrame implements MouseListener, ActionListener
{
    JPanel background, cg;
    int x, bdex;
    JLabel boxLabel, bLabel, a, points;

    JButton b1,b2,b3,b4,b5,b6,b7,b8, tb1 , tb2, temp, restart ;
    ImageIcon im, im2;
    
    ImageIcon b = new ImageIcon("5.png");
    ImageIcon a1 = new ImageIcon("1.png");
    ImageIcon a2 = new ImageIcon("2.png");
    ImageIcon a3 = new ImageIcon("3.png");
    ImageIcon a4 = new ImageIcon("4.png");

    ImageIcon res = new ImageIcon("r.png");


    ImageIcon gim, gim2;
    Image img,image;

    JButton []buttons = new JButton[8];
    ImageIcon []imcons = new ImageIcon[8];




    public Game()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(960,540);
        setLocationRelativeTo(null);
        setResizable(false);

        points = new JLabel("0");
        points.setFont(new Font("Exo 2", Font.PLAIN, 50));
        points.setBounds(5,5,100,60);
        points.setForeground(Color.WHITE);

        restart = new JButton(res);
 
        restart.setBounds(740,5,200,55);
        
        restart.addActionListener(this);

        restart.setContentAreaFilled(false);



        imconAssign();
        
        butSet();
        
        panelSet();
        
        butDis();

        background.add(bLabel);

        arrange();

        

    }

    public void arrange()
    {
        int index;
        ImageIcon t;
        Random random = new Random();
        for (int i = imcons.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            t = imcons[index];
            imcons[index] = imcons[i];
            imcons[i] = t;
        }

        for(int i=0;i<8;i++)
        {
            buttons[i].setVisible(true);
        }
    }
    
    public void imconAssign()
    {
        imcons[0]=a1;
        imcons[1]=a4;
        imcons[2]=a2;
        imcons[3]=a3;
        imcons[4]=a1;
        imcons[5]=a3;
        imcons[6]=a2;
        imcons[7]=a4;

    }

    public void panelSet()
    {
        background = new JPanel();
        background.setLayout(null);
        background.setBounds(0,0,960,540);

        im = new ImageIcon("b.png");

        img=im.getImage();
        image=img.getScaledInstance(this.getWidth()+5,this.getHeight()+5,Image.SCALE_SMOOTH);
        im2= new ImageIcon(image);

        

        bLabel = new JLabel(im2);
        bLabel.setBounds(0,0,965,545);

        background.add(restart);

        background.add(points);

        add(background);
        
    }


    public void butSet()
    {
        
        b1 = new JButton(b);
        b2 = new JButton(b);
        b3 = new JButton(b);
        b4 = new JButton(b);
        b5 = new JButton(b);
        b6 = new JButton(b);
        b7 = new JButton(b);
        b8 = new JButton(b);

        buttons[0]=b1;
        buttons[1]=b2;
        buttons[2]=b3;
        buttons[3]=b4;
        buttons[4]=b5;
        buttons[5]=b6;
        buttons[6]=b7;
        buttons[7]=b8;

    }


    public void butDis()
    {   
        int locs[][] = { {130,70}  , {305,70} , {480,70}, {655,70}, {130,245} , {305,245}, {480,245}, {655,245} };
        
        for (int i=0;i<8;i++)
        {
            for(int j = 0; j<1; j++)
            {
                buttons[i].setBounds(locs[i][j],locs[i][j+1],150,150);
                background.add(buttons[i]);
            }
        }

        b1.addMouseListener(this);
        b2.addMouseListener(this);
        b3.addMouseListener(this);
        b4.addMouseListener(this);
        b5.addMouseListener(this);
        b6.addMouseListener(this);
        b7.addMouseListener(this);
        b8.addMouseListener(this);
    }



    public void mouseEntered(MouseEvent me)
    {
        
    }
    public void mouseExited(MouseEvent me)
    {
       
       if(tb1!=null && tb2!=null)
       {
        tb1.setIcon(b);
        tb2.setIcon(b);
        tb1=null;
        tb2=null;
       }

    }
    public void mouseClicked(MouseEvent me)
    {   
        temp = (JButton)me.getSource();

       
       if(tb1==null)
        {
            tb1 =(JButton)me.getSource();
            for(int i = 0; i<8; i++)
            {
                if(buttons[i]==tb1)
                {
                    bdex=i;
                }
            }
            tb1.setIcon(imcons[bdex]);
        }
        else if(tb1==temp)
        {
            tb1.setIcon(b);
            //te.setIcon(b);
            tb1=null;
            //temp=null;
            int p = Integer.parseInt(points.getText());
                 p=p-1;
                 points.setText(""+p);
        }

        else if(tb2==null)
        {
            tb2 = (JButton)me.getSource();


            for(int i= 0; i<8 ; i++)
            {
                if(buttons[i]==tb2)
                {
                    bdex=i;
                }
            }
            tb2.setIcon(imcons[bdex]);

            if(tb1.getIcon()==tb2.getIcon())
            {
                tb1.setVisible(false);
                tb2.setVisible(false);
               
                

                int p = Integer.parseInt(points.getText());
                p=p+1;
                points.setText(""+p);


            }
            else
            {
                 int p = Integer.parseInt(points.getText());
                 p=p-1;
                 points.setText(""+p);

            }
        }
        
        else
        {
            tb1.setIcon(b);
            tb2.setIcon(b);
            tb1=null;
            tb2=null;
        }


    }
    public void mousePressed(MouseEvent me)
    {

    }
    public void mouseReleased(MouseEvent me)
    {
        
       
    }


    public void actionPerformed(ActionEvent ae)
    {
        
       if(tb1!=null)
       {
        tb1.setIcon(b);
        tb1=null;
       }
       if(tb2!=null)
       {
        tb2.setIcon(b);
        tb2=null;

       }

        int index;
        ImageIcon t;
        Random random = new Random();
        for (int i = imcons.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            t = imcons[index];
            imcons[index] = imcons[i];
            imcons[i] = t;
        }

        for(int i=0;i<8;i++)
        {
            buttons[i].setVisible(true);
        }

        points.setText("0");

    }
   
    public static void main(String[] args)
    {
        Game obj = new Game();
        obj.setVisible(true);
    }
}